<pre>
 ____
< hi >
 ----
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
</pre>

# MCJ

## Description

Code maker.

## Requirements

- Linux Laptop (Debian or Mint preferred)
- Quiet working conditions and a friendly atmosphere
- Coffee
- Pizza (optional)

## Installation

MCJ should start to work automatically when the requirements are met. If not, try adding more coffee.

## Known issues

- DOS can occur after brain overload
- may consume too much coffee from time to time